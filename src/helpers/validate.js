export const validateEmail = email => {
  const regexp = /^([a-z0-9_.-]+)@([\da-z.-]+)\.([a-z.]{2,6})$/gi;
  return email.match(regexp);
};

export const validatePassword = pas => {
  const regexp = /^[a-z0-9_-]{6,18}$/gi;
  return pas.match(regexp);
};

export const validateFullName = name => {
  const regexp = /(\w)+\s(\w)+/gi;
  return name.match(regexp);
};
