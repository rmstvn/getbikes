export let shouldRefresh = () => {
  console.log("should refresh fires");
  let now = new Date().getTime().toString(),
    refresh = JSON.parse(localStorage.getItem("expiresIn"));

  now = now.slice(0, now.length - 3);

  const res = refresh - now - 1000 < 0 ? true : false;
  console.log(res, now, refresh);
  return res;
};

export let checkAuth = () => {
  return localStorage.getItem("accessToken") &&
    localStorage.getItem("user") !== null
    ? true
    : false;
};

export let processDate = date => {
  const i = date.toString().indexOf("GMT");
  const res = date.toString().slice(0, i - 1);
  return res;
};
