import React, { Component } from "react";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";

export default class LinkTab extends Component {
  static propTypes = {
    label: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired,
    active: PropTypes.bool.isRequired,
    onChangeTab: PropTypes.func.isRequired
  };

  render() {
    const home =
      this.props.label === "Cities" ? (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="34"
          height="34"
          viewBox="0 0 34 34"
        >
          <path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z" />
          <path d="M0 0h24v24H0z" fill="none" />
        </svg>
      ) : null;
    const order =
      this.props.label === "Orders" ? (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="34"
          height="34"
          viewBox="0 0 34 34"
        >
          <path fill="none" d="M0 0h24v24H0V0z" />
          <path d="M21 3H3c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h18c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-9 8H3V9h9v2zm0-4H3V5h9v2z" />
        </svg>
      ) : null;
    const profile =
      this.props.label === "Profile" ? (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="34"
          height="34"
          viewBox="0 0 34 34"
        >
          <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z" />
          <path d="M0 0h24v24H0z" fill="none" />
        </svg>
      ) : null;

    return (
      <li
        className={`tab__list-item ${
          this.props.active ? "tab__list-item--active" : ""
        }`}
      >
        <NavLink
          to={{
            pathname: `/${this.props.to}`
          }}
          replace
          className="tab__link"
          onClick={() => this.props.onChangeTab(this.props.to)}
        >
          <div className="tab__icon-wrapper">{home || order || profile}</div>
          <div className="tab__label">{this.props.label}</div>
        </NavLink>
      </li>
    );
  }
}
