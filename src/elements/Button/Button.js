import React, { Component } from "react";
import PropTypes from "prop-types";

export default class Button extends Component {
  static propTypes = {
    type: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func,
    buttonText: PropTypes.string
  };

  render() {
    return (
      <button
        type={this.props.type}
        className={this.props.className}
        onClick={this.props.onClick}
      >
        {this.props.buttonText}
      </button>
    );
  }
}
