import React, { Component } from "react";
import PropTypes from "prop-types";

export default class Input extends Component {
  static propTypes = {
    required: PropTypes.bool,
    name: PropTypes.string,
    type: PropTypes.string,
    onChange: PropTypes.func,
    placeholder: PropTypes.string,
    value: PropTypes.string,
    className: PropTypes.string,
    text: PropTypes.string
  };

  static defaultProps = {
    required: false
  };

  render() {
    let className = "";
    if (
      this.props.type === "text" ||
      this.props.type === "password" ||
      this.props.type === "email" ||
      this.props.type === "search"
    ) {
      className = "input__text";
    } else if (
      this.props.type === "date" ||
      this.props.type === "time" ||
      this.props.type === "number"
    ) {
      className = "input__number";
    }

    return (
      <>
        {this.props.text && (
          <label className="label label__text-inp">{this.props.text}</label>
        )}
        <input
          type={this.props.type}
          {...this.props.required}
          name={this.props.name}
          onChange={this.props.onChange}
          placeholder={this.props.placeholder}
          defaultValue={this.props.value}
          className={`input ${className} ${
            this.props.className ? this.props.className : null
          }`}
        />
      </>
    );
  }
}
