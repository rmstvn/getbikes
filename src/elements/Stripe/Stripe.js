import React, { Component } from "react";
import PropTypes from "prop-types";
import StripeCheckout from "react-stripe-checkout";

export default class Stripe extends Component {
  static propTypes = {
    onToken: PropTypes.func,
    money: PropTypes.number
  };

  render() {
    return (
      <div>
        <StripeCheckout
          token={this.props.onToken}
          stripeKey={`${process.env.REACT_APP_STRIPE_PUB_KEY_COMMON}`}
          amount={this.props.money} // cents
          currency="EUR"
        />
      </div>
    );
  }
}
