import React, { Component } from "react";
import PropTypes from "prop-types";
import Input from "../Input/Input";

export default class SearchBar extends Component {
  static propTypes = {
    searchValue: PropTypes.string,
    onSearchChange: PropTypes.func,
    onSearchSubmit: PropTypes.func
  };

  render() {
    return (
      <>
        <form
          className="search-bar"
          onSubmit={e => this.props.onSearchSubmit(e)}
        >
          <Input
            text="Type a city"
            value={this.props.searchValue}
            type="search"
            required
            name="search"
            onChange={this.props.onSearchChange}
            placeholder="Type a city"
          />
          {/* <button className="btn btn-primary btn-s">Find</button> */}
        </form>
        {/* <ul className="search-results">{this.props.cities}</ul> */}
      </>
    );
  }
}
