import React, { Component } from 'react'

export default class ModalMessage extends Component {

  render() {
    const {type} = this.props;
    let className = '';
    if(type === "error"){
        className = "error"
    }

    return (
      <div className={className}>
        {this.props.children}
      </div>
    )
  }
}
