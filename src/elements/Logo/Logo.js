import React, { Component } from "react";
import LogoNormal from "../../assets/SVG/logo.svg";

export default class Logo extends Component {
  render() {
    return (
      <div className="logo__wrapper">
        <img src={LogoNormal} alt="Logo" />
        {/* <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 451.11 252.8">
          <defs />
          <title>Logo</title>
          <g id="Layer_2" data-name="Layer 2">
            <g id="Layer_1-2" data-name="Layer 1">
              <circle class="cls-1" cx="147" cy="203.8" r="44" />
              <path
                class="cls-2"
                d="M147,164.8a39,39,0,1,1-39,39,39.05,39.05,0,0,1,39-39m0-10a49,49,0,1,0,49,49,49,49,0,0,0-49-49Z"
              />
              <line class="cls-3" x1="49" y1="145.8" x2="49" y2="231.8" />
              <line class="cls-4" x1="49" y1="206.8" x2="103" y2="206.8" />
              <line class="cls-5" x1="115" y1="73.8" x2="178.89" y2="137.69" />
              <text
                class="cls-6"
                transform="matrix(0.93, -0.12, 0.01, 1, 177.73, 120.42)"
              >
                G
                <tspan class="cls-7" x="57.41" y="0">
                  ET
                </tspan>
                <tspan class="cls-7">
                  <tspan x="0" y="81">
                    {" "}
                  </tspan>
                  <tspan class="cls-8" x="20.75" y="81">
                    {" "}
                  </tspan>
                  <tspan x="32.61" y="81">
                    BIKES
                  </tspan>
                </tspan>
              </text>
              <circle class="cls-1" cx="49" cy="105.8" r="44" />
              <path
                class="cls-2"
                d="M49,66.8a39,39,0,1,1-39,39,39.05,39.05,0,0,1,39-39m0-10a49,49,0,1,0,49,49,49,49,0,0,0-49-49Z"
              />
            </g>
          </g>
        </svg>*/}
      </div>
    );
  }
}
