import React, { Component } from "react";
import PropTypes from "prop-types";

export default class Input extends Component {
  static propTypes = {
    required: PropTypes.bool,
    name: PropTypes.string,
    type: PropTypes.string,
    onChange: PropTypes.func,
    placeholder: PropTypes.string,
    defaultValue: PropTypes.string,
    className: PropTypes.string,
    text: PropTypes.string,
    max: PropTypes.number,
    min: PropTypes.number
  };

  render() {
    let className = "";
    if (
      this.props.type === "date" ||
      this.props.type === "time" ||
      this.props.type === "number"
    ) {
      className = "input__number";
    }

    return (
      <>
        <label className="label label__num-inp">{this.props.text}</label>
        <input
          type={`${this.props.type}`}
          {...this.props.required}
          name={`${this.props.name}`}
          onChange={e => this.props.onChange(e)}
          placeholder={this.props.placeholder}
          defaultValue={this.props.defaultValue}
          value={this.props.value}
          max={this.props.max}
          min={this.props.min}
          className={`input ${className} ${this.props.className}`}
        />
      </>
    );
  }
}
