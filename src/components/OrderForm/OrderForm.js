import React, { Component } from "react";
import PropTypes from "prop-types";
import Button from "../../elements/Button/Button";
import InputNum from "../../elements/InputNum/InputNum";

export default class OrderForm extends Component {
  static propTypes = {
    handleSubmitOrder: PropTypes.func,
    onInputChange: PropTypes.func,
    bikeAmount: PropTypes.number
  };

  render() {
    return (
      <div>
        <form
          className="form-order to-floor"
          onSubmit={e => this.props.handleSubmitOrder(e)}
        >
          <div className="input-gpoup">
            <fieldset className="order__fieldset">
              <legend>When do you want to start?</legend>
              <InputNum
                text="Day"
                value={this.props.dayToStart}
                type="date"
                required
                name="dayToStart"
                onChange={this.props.onInputChange}
                className="order-form__inputNum"
              />
              <InputNum
                text="Time"
                isLabel={false}
                value={this.props.timeToStart}
                type="time"
                required
                name="timeToStart"
                onChange={this.props.onInputChange}
              />
            </fieldset>

            <fieldset className="order__fieldset">
              <legend>How long do you want to ride?</legend>

              <InputNum
                text="Hours"
                value={this.props.timeToEnd}
                type="number"
                min={1}
                max={3}
                required
                name="timeToEnd"
                onChange={this.props.onInputChange}
              />
            </fieldset>
            <fieldset className="order__fieldset">
              <legend className="form-order">
                How many bikes do you want to use?
              </legend>
              <InputNum
                text="Bikes"
                value={this.props.bikeAmount}
                type="number"
                required
                min={1}
                max={5}
                name="bikeAmount"
                onChange={this.props.onInputChange}
              />
            </fieldset>
          </div>
          <div className="btn-group order__btn">
            <Button
              buttonText="Find me Bikes"
              type="submit"
              className="btn btn-primary btn-l full-w"
            />
          </div>
        </form>
      </div>
    );
  }
}
