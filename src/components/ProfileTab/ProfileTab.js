import React, { Component } from "react";
import Input from "../../elements/Input/Input";
import WebService from "../../services/webService";

export default class ProfileTab extends Component {
  api = new WebService();

  state = {
    onEdit: false,
    fullName: JSON.parse(localStorage.getItem("user")).fullName,
    email: JSON.parse(localStorage.getItem("user")).email,
    onChangedInput: false
  };

  onEdit = () => {
    this.setState(({ onEdit }) => {
      return {
        onEdit: !onEdit
      };
    });
  };

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    if (this.state.onChangedInput) {
      this.api
        .req(
          undefined,
          `/users/object/${JSON.parse(localStorage.getItem("user")).id}/update`,
          "POST",
          {
            fullName: this.state.fullName,
            email: this.state.email
          }
        )
        .then(data => {
          console.log(data);
          localStorage.setItem("user", JSON.stringify(data));
          this.setState(({ onEdit }) => {
            return {
              onEdit: !onEdit
            };
          });
        });
    }
  };
  render() {
    return (
      <div className="profile">
        <div className="profile__photo"> Photo will be here! </div>
        {!this.state.onEdit ? (
          <div className="profile__info">
            <div className="profile__name">
              {JSON.parse(localStorage.getItem("user")).fullName}
            </div>
            <div className="profile__email">
              {JSON.parse(localStorage.getItem("user")).email}
            </div>
            <button className="btn btn-primary btn-s" onClick={this.onEdit}>
              Edit info
            </button>
          </div>
        ) : null}

        {this.state.onEdit ? (
          <div className="profile__info">
            <form onSubmit={e => this.onSubmit(e)}>
              <Input
                className="profile__name"
                type="text"
                name="fullName"
                value={JSON.parse(localStorage.getItem("user")).fullName}
                onChange={this.onChange}
              />

              <Input
                className="profile__email"
                type="email"
                name="email"
                value={JSON.parse(localStorage.getItem("user")).email}
              />

              <button
                className="btn btn-primary btn-s"
                disable={this.state.onChangedInput}
              >
                Edit info
              </button>
            </form>
          </div>
        ) : null}
      </div>
    );
  }
}
