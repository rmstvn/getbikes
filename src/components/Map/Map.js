import React, { Component } from "react";
import PropTypes from "prop-types";
import MapService from "../../services/leaflet";
import WebService from "../../services/webService";

export default class Map extends Component {
  static propTypes = {
    markers: PropTypes.array,
    activeCityLocation: PropTypes.object,
    onChangeActiveCity: PropTypes.func
  };

  componentDidMount() {
    this.mapService = new MapService();
    this.api = new WebService();

    this.map = this.mapService.initMap();
    this.mapService.initTitle(this.map);
    this.layer = this.mapService.addLayer(this.map);
    this.mapService.updateMarkers(this.props.markers, this.layer);

    this.api.getCurrentLocation(this.map, (map, { latitude, longitude }) => {
      this.api.getCityByLocation(latitude, longitude).then(data => {
        console.log(data);
        this.props.onChangeActiveCity(data.address.city, {
          lat: data.lat,
          lon: data.lon
        });
        return data.address.city;
      });
    });

    // this.api.getCityByLocation(50, 50);
  }

  componentDidUpdate(props) {
    const location = {
      latitude: this.props.activeCityLocation.lat,
      longitude: this.props.activeCityLocation.lon
    };

    console.log(this.props, props, location);

    if (this.props.activeCityLocation !== props.activeCityLocation) {
      this.mapService.setView(this.map, location);
    }
    if (this.props.markers !== props.markers) {
      this.mapService.updateMarkers(this.props.markers, this.layer);
    }
  }

  componentWillUnmount() {}

  render() {
    return (
      <div id="map" className="map-overlay">
        {" "}
      </div>
    );
  }
}
