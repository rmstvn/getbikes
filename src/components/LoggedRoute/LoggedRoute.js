import React from "react";
import { Route, Redirect } from "react-router-dom";
import { checkAuth } from "../../helpers/helpers";

export const LoggedRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props => {
        if (checkAuth()) {
          return <Component {...props} />;
        } else {
          return (
            <Redirect
              to={{
                pathname: "/sign",
                state: {
                  from: props.location
                }
              }}
            />
          );
        }
      }}
    />
  );
};
