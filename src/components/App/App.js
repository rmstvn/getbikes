import React, { Component } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import AuthPage from "../AuthPage/AuthPage";
import MainPage from "../MainPage/MainPage";
import NotFound from "../NotFound/NotFound";
import { LoggedRoute } from "../LoggedRoute/LoggedRoute";
import { checkAuth } from "../../helpers/helpers";

export default class Router extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route
            exact
            path="/sign"
            render={props => {
              if (checkAuth()) {
                return (
                  <Redirect
                    to={{
                      pathname: "/cities",
                      state: {
                        from: props.location
                      }
                    }}
                  />
                );
              } else {
                return <AuthPage {...props} />;
              }
            }}
          />
          <LoggedRoute path="/:tabID" component={MainPage} />
          <Route
            path="/"
            render={props => {
              if (checkAuth()) {
                return (
                  <Redirect
                    to={{
                      pathname: "/cities",
                      state: {
                        from: props.location
                      }
                    }}
                  />
                );
              } else {
                return <AuthPage {...props} />;
              }
            }}
          />
          <Route component={NotFound} />
        </Switch>
      </BrowserRouter>
    );
  }
}
