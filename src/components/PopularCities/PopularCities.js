import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

export default class PopularCities extends Component {
  static propTypes = {
    cities: PropTypes.array,
    activeCityID: PropTypes.number,
    match: PropTypes.object,
    onChangeActiveCity: PropTypes.func
  };

  render() {
    return (
      <div className="popular-cities">
        {this.props.cities.map(city => (
          <Link
            key={city.id}
            to={{
              pathname: `${this.props.match.url}/${city.city
                .replace(" ", "-")
                .toLowerCase()}`
            }}
            index={city.id}
            className={`popular-cities__item ${
              this.props.activeCityID === city.id
                ? "popular-cities__item--active"
                : null
            }`}
            onClick={() => {
              return this.props.onChangeActiveCity(city.city, city.location);
            }}
          >
            {city.city}
          </Link>
        ))}
        {/* I made it before I had changed it to Link
        <span
            key={city.id}
            index={city.id}
            className={`popular-cities__item ${
              this.props.activeCityID === city.id
                ? "popular-cities__item--active"
                : null
            }`}
            onClick={() => {
              console.log(city);
              return this.props.onChangeActiveCity(city.city, city.location);
            }}
          >
            {city.city}
          </span> */}
      </div>
    );
  }
}
