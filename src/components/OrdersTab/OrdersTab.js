import React, { Component } from "react";
import PropTypes from "prop-types";
import Order from "../Order/Order";

export default class OrdersTab extends Component {
  static propTypes = {
    orders: PropTypes.array,
    onOrderChange: PropTypes.func
  };

  render() {
    return (
      <div className="orders-list">
        {this.props.orders
          .map(order => {
            console.log(order);
            return (
              <Order
                key={order.id}
                status={order.status}
                id={order.id}
                onOrderChange={this.props.onOrderChange}
                city={order.city}
                amount={order.amount}
                startDate={order.startDate}
                endDate={order.endDate}
              />
            );
          })
          .sort((a, b) => {
            console.log(a, b);
            return a.props.startDate < b.props.startDate;
          })}
      </div>
    );
  }
}
