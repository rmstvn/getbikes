import React, { Component } from "react";
import Logo from "../../elements/Logo/Logo";
import AuthForm from "../AuthForm/AuthForm";

// import styles from "./Auth.module.css";

export default class Auth extends Component {
  render() {
    return (
      <div className="main-container">
        <div className="control-side auth-container">
          <Logo />
          <AuthForm />
        </div>
        <div className={`signIllustration`} />{" "}
      </div>
    );
  }
}
