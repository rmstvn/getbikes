import React, { Component } from "react";
import PropTypes from "prop-types";
import OrderForm from "../OrderForm/OrderForm";
import SearchBar from "../../elements/SearchBar/SearchBar";
import PopularCities from "../PopularCities/PopularCities";
import StorePrev from "../StorePrev/StorePrev";
import Stripe from "../../elements/Stripe/Stripe";

export default class CitiesTab extends Component {
  static propTypes = {
    activeCenterID: PropTypes.number,
    activeCity: PropTypes.object,
    bikeAmount: PropTypes.number,
    centers: PropTypes.array,
    cities: PropTypes.array,
    dayToStart: PropTypes.string,
    handleSubmitOrder: PropTypes.func,
    history: PropTypes.object,
    location: PropTypes.object,
    match: PropTypes.object,
    money: PropTypes.number,
    onChangeActiveCenter: PropTypes.func,
    onChangeActiveCity: PropTypes.func,
    onInputChange: PropTypes.func,
    onSearchChange: PropTypes.func,
    onSearchSubmit: PropTypes.func,
    onToken: PropTypes.func,
    orderFormCompleted: PropTypes.bool,
    searchValue: PropTypes.string,
    timeToEnd: PropTypes.string,
    timeToStart: PropTypes.string
  };

  state = {
    orderFrom: false
  };

  onShowOrderFrom = id => {
    this.props.onChangeActiveCenter(id);
    this.setState({
      orderFrom: true
    });
  };
  render() {
    console.log(this.props);
    return (
      <div className="city">
        <SearchBar
          searchValue={this.props.searchValue}
          onSearchChange={this.props.onSearchChange}
          onSearchSubmit={this.props.onSearchSubmit}
          cities={this.props.cities}
        />
        <PopularCities
          cities={this.props.cities}
          activeCityID={this.props.activeCity.id}
          match={this.props.match}
          onChangeActiveCity={this.props.onChangeActiveCity}
        />

        <h1 className="city__title">{this.props.activeCity.name}</h1>

        {this.props.centers.map(center => {
          return (
            <StorePrev
              key={center.id}
              id={center.id}
              title={center.name}
              cost={center.cost}
              onShowOrderForm={this.onShowOrderFrom}
            />
          );
        })}

        {this.state.orderFrom ? (
          <>
            <OrderForm
              activeCenterID={this.props.activeCenterID}
              dayToStart={this.props.dayToStart}
              timeToStart={this.props.timeToStart}
              timeToEnd={this.props.timeToEnd}
              bikeAmount={this.props.bikeAmount}
              onInputChange={this.props.onInputChange}
              handleSubmitOrder={this.props.handleSubmitOrder}
            />
            {this.props.orderFormCompleted ? (
              <div className="stripe">
                <Stripe onToken={this.props.onToken} money={this.props.moeny} />
              </div>
            ) : null}
          </>
        ) : null}
      </div>
    );
  }
}
