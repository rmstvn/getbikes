import React, { Component } from "react";
import PropTypes from "prop-types";
import { processDate } from "../../helpers/helpers";

export default class Order extends Component {
  static propTypes = {
    status: PropTypes.string,
    id: PropTypes.string,
    city: PropTypes.string,
    amount: PropTypes.number,
    onOrderChange: PropTypes.func,
    startDate: PropTypes.string,
    endDate: PropTypes.string
  };

  processDate;

  renderConfirmed = () => {
    if (this.props.status === "confirmed") {
      return (
        <div className="order order--confirmed">
          <div className="order__title">Let's activate your order!</div>
          <div className="order__detail">
            <div>
              City:{" "}
              <span className="fw500">{this.props.city.toUpperCase()}</span>
            </div>
            <div>
              Total costs:{" "}
              <span className="fw500">{this.props.amount / 100} $</span>
            </div>
            <div>
              Your order starts{" "}
              <span className="fw500">{`${processDate(
                new Date(this.props.startDate)
              )}`}</span>
            </div>
            <div>
              Your order finish{" "}
              <span className="fw500">{`${processDate(
                new Date(this.props.endDate)
              )}`}</span>
            </div>
          </div>

          <button
            className="btn btn-s btn-success order__activate"
            onClick={() =>
              this.props.onOrderChange(this.props.id, this.props.status)
            }
          >
            Activate
          </button>
        </div>
      );
    }
  };

  renderActive = () => {
    if (this.props.status === "active") {
      return (
        <div className="order order--active">
          <div className="order__title">Your order is running now!</div>
          <div className="order__detail">
            <div>
              Your order finish{" "}
              <span className="fw500">{`${processDate(
                new Date(this.props.endDate)
              )}`}</span>
            </div>
            <div>
              Total costs:{" "}
              <span className="fw500">{this.props.amount / 100} $</span>
            </div>
          </div>

          <button
            className="btn btn-s btn-primary order__finish"
            onClick={() =>
              this.props.onOrderChange(this.props.id, this.props.status)
            }
          >
            Finish
          </button>
        </div>
      );
    }
  };

  renderDone = () => {
    if (this.props.status === "completed") {
      return (
        <div className="order order--done">
          <div className="order__title">Your order has been finished</div>
          <div className="order__detail">
            <div>
              Total costs:{" "}
              <span className="fw500">{this.props.amount / 100} $</span>
            </div>
            <div>
              Your order has been finished{" "}
              <span className="fw500">{`${processDate(
                new Date(this.props.endDate)
              )}`}</span>
            </div>
          </div>
        </div>
      );
    }
  };
  render() {
    return (
      <>
        {this.renderConfirmed()}
        {this.renderActive()}
        {this.renderDone()}
      </>
    );
  }
}
