import React, { Component } from "react";
import Button from "../../elements/Button/Button";
import Input from "../../elements/Input/Input";
import WebService from "../../services/webService";
import { withRouter } from "react-router-dom";
import {
  validateEmail,
  validatePassword,
  validateFullName
} from "../../helpers/validate";

import ModalMessage from "../../elements/ModalMessage/ModalMessage";

class AuthForm extends Component {
  api = new WebService();

  state = {
    signin: true,
    signup: false,
    error: false,
    email: "",
    password: "",
    fullName: ""
  };

  onInputChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSignIn = e => {
    e.preventDefault();
    if (
      validateEmail(this.state.email) &&
      validatePassword(this.state.password)
    ) {
      const body = {
        email: this.state.email,
        password: this.state.password
      };

      this.api
        .req(undefined, "/auth/signin", "POST", body)
        .then(data => {
          if (data.access_token) {
            localStorage.setItem("accessToken", data.access_token);
            localStorage.setItem("user", JSON.stringify(data.Item));
            localStorage.setItem("expiresIn", data.expires_in);
            localStorage.setItem("refreshToken", data.Item.refresh_token);

            return data;
          } else {
            throw new Error(data.err.message);
          }
        })
        .then(() => {
          this.props.history.push("/cities");
        })
        .catch(err => {
          this.setState({
            error: err
          });
        });
    } else {
      this.setState({
        error: true
      });
    }
  };

  onSignUp = e => {
    e.preventDefault();
    if (
      validateEmail(this.state.email) &&
      validatePassword(this.state.password) &&
      validateFullName(this.state.fullName)
    ) {
      const body = {
        email: this.state.email,
        password: this.state.password,
        fullName: this.state.fullName
      };

      this.api
        .req(undefined, "/auth/signup", "POST", body)
        .then(data => {
          if (data.access_token) {
            localStorage.setItem("accessToken", data.access_token);
            localStorage.setItem("user", JSON.stringify(data.Item));
            localStorage.setItem("expiresIn", data.expires_in);
            localStorage.setItem("refreshToken", data.Item.refresh_token);

            return data;
          } else {
            throw new Error(data.err.message);
          }
        })
        .then(() => {
          this.props.history.push("/cities");
        })
        .catch(err => {
          this.setState({
            error: err
          });
        });
    } else {
      this.setState({
        error: true
      });
    }
  };

  handleSignUp = () => {
    this.setState({
      signin: false,
      signup: true
    });
  };

  render() {
    const signIn = this.state.signin && !this.state.signup;
    const signUp = !this.state.signin && this.state.signup;

    return (
      <form
        className="sign-form to-floor"
        onSubmit={signIn ? this.onSignIn : this.onSignUp}
      >
        <div className="sign-form__input-group">
          <div className="sign-form__input">
            <Input
              text="Email"
              value={this.state.email}
              type="text"
              name="email"
              required
              onChange={this.onInputChange}
              placeholder="human@gmail.com"
            />
          </div>
          <div className="sign-form__input">
            <Input
              text="Password"
              value={this.state.password}
              type="password"
              required
              name="password"
              onChange={this.onInputChange}
              className="input-txt"
              placeholder="secret"
            />
          </div>

          {signUp && (
            <div className="sign-form__input">
              <Input
                text="Full Name"
                value={this.state.fullName}
                type="text"
                required
                name="fullName"
                onChange={this.onInputChange}
                placeholder="Name Surname"
              />{" "}
            </div>
          )}
          {this.state.error === true && (
            <ModalMessage type="error">
              You're first time here? If so, you can sign up. Or maybe here is
              little misspelling in email or password.
            </ModalMessage>
          )}

          {this.state.error instanceof String && (
            <ModalMessage type="error">{this.state.error}</ModalMessage>
          )}
        </div>

        <div className="btn-group ">
          {signIn && (
            <Button
              buttonText="Sign In"
              type="submit"
              className="btn btn-primary half-w btn-l mr20 "
            />
          )}

          <Button
            buttonText="Sign Up"
            type={`${signUp ? "submit" : "button"}`}
            className={`btn btn-transparent btn-l  ${
              signUp ? "full-w" : "half-w"
            }`}
            onClick={this.handleSignUp}
          />
        </div>
      </form>
    );
  }
}

export default withRouter(AuthForm);
