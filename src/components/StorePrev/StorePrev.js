import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";

class StorePrev extends Component {
  static propTypes = {
    id: PropTypes.number,
    onShowOrderForm: PropTypes.func,
    title: PropTypes.string,
    cost: PropTypes.number
  };

  render() {
    return (
      <div className="store-prev">
        <div className="store-prev__title">{this.props.title}</div>
        <div className="store-prev__wrapper">
          <div className="store-prev__info-wrapper">
            <div className="store-prev__info">
              <span className="opacity-50">Adress</span> Heroiv Pratsi, 2
            </div>
            <div className="store-prev__info">
              <span className="opacity-50">Bikes</span>
              <span className="fw500"> 50</span> bikes are available
            </div>
            <div className="store-prev__info">
              <span className="opacity-50">Cost</span>
              <span className="fw500"> {this.props.cost}</span>
            </div>
          </div>

          <button
            className="btn btn-primary btn-s"
            onClick={() => this.props.onShowOrderForm(this.props.id)}
          >
            Get Bikes{" "}
          </button>

          {/* I tried to use routing, but I failed. Here is the part of global idea
          
          <Link
            to={{
              pathname: `${this.props.match.url}/${this.props.title
                .replace(" ", "-")
                .toLowerCase()}`
            }}
            className="btn btn-primary btn-s"
            onClick={() => {
              this.props.onShowOrderFrom();
              this.props.onChangeActiveCenter(this.props.id);
            }}
          >
            {" "}
            Get Bikes{" "}
          </Link> */}
        </div>
      </div>
    );
  }
}

export default withRouter(StorePrev);
