import React, { Component } from "react";
import PropTypes from "prop-types";
import Map from "../Map/Map";
import CitiesTab from "../CititesTab/CitiesTab";
import OrdersTab from "../OrdersTab/OrdersTab";
import ProfileTab from "../ProfileTab/ProfileTab";
import Button from "../../elements/Button/Button";
import WebService from "../../services/webService";
import LinkTab from "../../elements/LinkTab/LinkTab";
import { Route } from "react-router-dom";
import MapService from "../../services/leaflet";

export default class MainPage extends Component {
  api = new WebService();
  mapService = new MapService();

  static propTypes = {
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired
  };

  state = {
    tabs: [
      {
        label: "Cities",
        to: "cities"
      },
      {
        label: "Orders",
        to: "orders"
      },
      {
        label: "Profile",
        to: "profile"
      }
    ],
    markersData: [],
    cities: [],
    activeTab: "",
    activeCityName: "",
    activeCityId: null,
    activeCityLocation: {
      lat: null,
      lon: null
    },
    centers: [],
    activeCenterID: null,
    dayToStart: "",
    timeToStart: "",
    timeToEnd: "",
    bikeAmount: 1,
    orderFormCompleted: false,
    orders: [],
    search: ""
  };

  componentDidMount() {
    this.api.getCities(cities => {
      console.log("get cities - done");
      this.setState({
        cities
      });
    });

    this.api.getCenters(this.state.activeCityName, centers => {
      console.log("get centers done", centers);
      this.setState({
        centers
      });

      centers.map(center => {
        console.log("map centers");
        this.setState({
          markersData: {
            lat: center.location.lat,
            lon: center.location.lon,
            title: `${center.name}, ${center.location.address}`
          }
        });
      });
    });

    this.api
      .req(
        undefined,
        `/bookings/user/${
          JSON.parse(localStorage.getItem("user")).id
        }/status/all`,
        "GET"
      )
      .then(orders => {
        this.setState({
          orders: orders
        });
      });

    this.setState({
      activeTab: this.props.match.params.tabID
    });
  }

  onLogout = () => {
    localStorage.clear();
    document.location.reload();
  };

  onChangeTab = tabID => {
    this.setState({
      activeTab: tabID
    });
  };

  onInputChange = e => {
    e.persist();
    console.log(e.target);
    this.setState(() => {
      return {
        [e.target.name]: e.target.value
      };
    });
  };

  handleSubmitOrder = e => {
    e.preventDefault();

    const start = `${this.state.dayToStart}+${this.state.timeToStart}`;
    const timeToEnd = `${parseInt(this.state.timeToStart.slice(0, 3)) +
      parseInt(this.state.timeToEnd)}:${this.state.timeToStart.slice(3, 5)}`;
    const end = `${this.state.dayToStart}+${timeToEnd}:00`;

    console.log(start, timeToEnd, end);

    this.api
      .req(
        undefined,
        `/bookings/cost?start=${start}&end=${end}&centerId=${
          this.state.activeCenterID
        }&bikesAmount=${this.state.bikeAmount}`,
        "GET",
        undefined,
        localStorage.getItem("accessToken")
      )
      .then(data => {
        this.setState({
          endTimestamp: data.endTimestamp,
          startTimestamp: data.startTimestamp,
          money: data.bookingCostGeneral,
          orderFormCompleted: true
        });
      });
  };

  onChangeActiveCity = (name, location) => {
    this.setState(
      {
        activeCityName: name,
        activeCityLocation: {
          lat: parseFloat(location.lat),
          lon: parseFloat(location.lon)
        }
      },
      () => {
        if (this.state.cities.length > 0) {
          const activeCityId = this.state.cities.filter(
            city => city.city === this.state.activeCityName
          );

          console.log(activeCityId, "active City id");
          this.setState({
            activeCityId: activeCityId[0].id
          });
        }

        this.api
          .req(
            undefined,
            `/centers/${this.state.activeCityName}`,
            "GET",
            false,
            localStorage.getItem("accessToken")
          )
          .then(data => {
            console.log("centers of new active city");
            const markers = data.Items.map(center => {
              console.log("map centers");

              return {
                lat: parseFloat(center.location.lat),
                lon: parseFloat(center.location.lon),
                title: `${center.name}, ${center.location.address}`
              };
            });
            console.log(markers);
            this.setState({
              centers: data.Items,
              markersData: markers
            });
          });
      }
    );
  };

  onToken = token => {
    console.log(token);
    this.api
      .req(
        undefined,
        `/bookings/${this.state.activeCityName.toLocaleLowerCase()}`,
        "POST",
        {
          token: token.id,
          amount: `${this.state.money}`,
          centerId: `${this.state.activeCenterID}`,
          bikesAmount: +this.state.bikeAmount,
          startTime: +this.state.startTimestamp,
          endTime: +this.state.endTimestamp
        }
      )
      .then(order => {
        this.setState(({ orders }) => {
          return {
            orders: [...orders, order.booking]
          };
        });
      });

    this.props.history.push("orders");
  };

  onChangeActiveCenter = id => {
    this.setState({
      activeCenterID: parseInt(id)
    });
  };

  onOrderChange = (id, status) => {
    let todo;
    if (status === "confirmed") {
      todo = "pickup";
    }
    if (status === "active") {
      todo = "dropoff";
    }
    this.api.req(undefined, `/bookings/${id}/${todo}`, "POST").then(data => {
      this.setState(({ orders }) => {
        const order = orders.filter(order => order.id === id);
        order[0].status = data.status;

        return {
          orders: [...orders]
        };
      });
    });
  };

  onSearchChange = e => {
    this.setState(() => {
      return {
        [e.target.name]: e.target.value
      };
    });
  };

  onSearchSubmit = e => {
    e.preventDefault();

    this.setState({
      activeCityName: e.target.value
    });
  };

  render() {
    return (
      <div className="flex vh100 front-page">
        <div className="control-side">
          <div className="tabs">
            <ol className="tab-list">
              {this.state.tabs.map(tab => {
                return (
                  <LinkTab
                    key={tab.to}
                    label={tab.label}
                    to={tab.to}
                    active={this.state.activeTab === tab.to ? true : false}
                    onChangeTab={this.onChangeTab}
                  />
                );
              })}
            </ol>
          </div>

          <div className="tab-content">
            <Route
              path="/cities"
              render={props => (
                <CitiesTab
                  {...props}
                  cities={this.state.cities}
                  activeCity={{
                    id: this.state.activeCityId,
                    name: this.state.activeCityName
                  }}
                  onChangeActiveCity={this.onChangeActiveCity}
                  centers={this.state.centers}
                  onChangeActiveCenter={this.onChangeActiveCenter}
                  activeCenterID={this.state.activeCenterID}
                  dayToStart={this.state.dayToStart}
                  timeToStart={this.state.timeToStart}
                  timeToEnd={this.state.timeToEnd}
                  bikeAmount={this.state.bikeAmount}
                  onInputChange={this.onInputChange}
                  handleSubmitOrder={this.handleSubmitOrder}
                  onToken={this.onToken}
                  money={this.state.money}
                  orderFormCompleted={this.state.orderFormCompleted}
                  onSearchSubmit={this.onSearchSubmit}
                  onSearchChange={this.onSearchChange}
                  searchValue={this.state.search}
                />
              )}
            />

            <Route
              path="/orders"
              component={props => (
                <OrdersTab
                  {...props}
                  orders={this.state.orders}
                  onOrderChange={this.onOrderChange}
                />
              )}
            />
            <Route path="/profile" component={ProfileTab} />
          </div>
        </div>
        <Button
          buttonText="Log Out"
          className="btn btn-s btn-danger app-logout"
          onClick={this.onLogout}
        />

        <Map
          markers={this.state.markersData}
          activeCityLocation={this.state.activeCityLocation}
          onChangeActiveCity={this.onChangeActiveCity}
        />
      </div>
    );
  }
}
