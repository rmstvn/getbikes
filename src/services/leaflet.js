import L from "leaflet";
import "leaflet/dist/leaflet.css";
import marker from "../assets/SVG/Pin.svg";

export default class MapService {
  constructor() {
    this.getBikesMarker = L.icon({
      iconUrl: marker,

      iconSize: [108, 62], // size of the icon
      iconAnchor: [100, 100], // point of the icon which will correspond to marker's location
      shadowAnchor: [4, 62], // the same for the shadow
      popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
    });
  }

  initMap = (name = "map", lat = 48.2, lon = 16.366667) => {
    return L.map(name, {
      center: [lat, lon],
      zoom: 16
    });
  };

  initTitle = map => {
    L.tileLayer(
      `https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=${
        process.env.REACT_APP_MAP_ACCESS_TOKEN
      }`,
      {
        attribution:
          'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: "mapbox.streets",
        accessToken: `${process.env.REACT_APP_MAP_ACCESS_TOKEN}`
      }
    ).addTo(map);
  };

  setView = (map, location) => {
    console.log("set view fires", location);
    map.panTo(L.latLng(location.latitude, location.longitude));
  };

  addLayer = map => {
    return L.layerGroup().addTo(map);
  };

  updateMarkers = (markers, layer) => {
    layer.clearLayers();
    markers.forEach(marker => {
      console.log(marker);
      L.marker(
        [marker.lat, marker.lon],
        { icon: this.getBikesMarker },
        { title: marker.title }
      ).addTo(layer);
    });
  };
}
