import { shouldRefresh } from "../helpers/helpers";

export default class WebService {
  constructor() {
    this._apiBase =
      "https://0uumsbtgfd.execute-api.eu-central-1.amazonaws.com/Development/v0";
    this._geocoding =
      "https://nominatim.openstreetmap.org/reverse?format=json&";
  }

  req = async (
    urlBase = this._apiBase,
    url,
    method = "GET",
    body,
    accessToken = localStorage.getItem("accessToken")
  ) => {
    console.log(url);
    if (shouldRefresh()) {
      this.refreshToken();
    }

    const options = {
      method: `${method}`,
      headers: {
        "Content-type": "application/json"
      }
    };
    if (body) {
      console.log("req body", body);
      options.body = JSON.stringify(body);
    }
    if (accessToken) {
      console.log("accessToken in req");

      options.headers = {
        Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
        "Content-type": "application/json"
      };
    }

    // console.log("req options", options);

    const res = await fetch(`${urlBase}${url}`, options)
      .then(this.checkError)
      .then(response => response.json())
      .catch(err => {
        console.log(err.status);
      });
    console.log(res);
    return res;
  };

  getCityByLocation = async (lat, lon) => {
    const res = await this.req(
      this._geocoding,
      `lat=${lat}&lon=${lon}&accept-language=en-US`
    );
    return res;
  };

  getCurrentLocation = async (map, cb) => {
    await navigator.geolocation.getCurrentPosition(pos => {
      if (cb) {
        cb(map, pos.coords);
      }
      return pos.coords;
    });
  };

  refreshToken = async () => {
    console.log(shouldRefresh());
    let res;
    const body = {
      refresh_token: localStorage.getItem("refreshToken")
    };

    console.log("refresh fires");
    res = await fetch(`${this._apiBase}/auth/refresh`, {
      method: "POST",
      body: JSON.stringify(body),
      headers: {
        "Content-type": "application/json"
      }
    })
      .then(this.checkError)
      .then(response => response.json())
      .then(data => {
        console.log(data, "refresh success");
        localStorage.setItem("accessToken", data.access_token);
        localStorage.setItem("expiresIn", data.expires_in);
        localStorage.setItem("refreshToken", data.user.refresh_token);
      })
      .catch(err => {
        console.log(err);
      });

    return res;
  };

  checkError = data => {
    if (!data.ok) {
      console.log(data);
      if (data.status === 403) {
        console.log("status 403");
        this.refreshToken();
      }
      throw new Error(data);
    }
    return data;
  };

  getCities = cb => {
    this.req(
      undefined,
      "/cities",
      "GET",
      false,
      localStorage.getItem("accessToken")
    )
      .then(data => {
        if (data instanceof Error) {
          throw new Error(data);
        }
        cb(data.Items);
      })
      .catch(err => {
        console.log(err);
      });
  };

  getCenters = (city, cb) => {
    this.req(
      undefined,
      `/centers/${city}`,
      "GET",
      false,
      localStorage.getItem("accessToken")
    )
      .then(data => {
        if (data instanceof Error) {
          throw new Error(data);
        }

        console.log(data.Items);

        cb(data.Items);
      })
      .catch(err => {
        console.log(err);
      });
  };
}
